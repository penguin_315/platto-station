/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createMemberStatus = /* GraphQL */ `
  mutation CreateMemberStatus(
    $input: CreateMemberStatusInput!
    $condition: ModelMemberStatusConditionInput
  ) {
    createMemberStatus(input: $input, condition: $condition) {
      id
      name
      owner
      comment
      status
      talkable
      timestamp
    }
  }
`;
export const updateMemberStatus = /* GraphQL */ `
  mutation UpdateMemberStatus(
    $input: UpdateMemberStatusInput!
    $condition: ModelMemberStatusConditionInput
  ) {
    updateMemberStatus(input: $input, condition: $condition) {
      id
      name
      owner
      comment
      status
      talkable
      timestamp
    }
  }
`;
export const deleteMemberStatus = /* GraphQL */ `
  mutation DeleteMemberStatus(
    $input: DeleteMemberStatusInput!
    $condition: ModelMemberStatusConditionInput
  ) {
    deleteMemberStatus(input: $input, condition: $condition) {
      id
      name
      owner
      comment
      status
      talkable
      timestamp
    }
  }
`;
