/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getMemberStatus = /* GraphQL */ `
  query GetMemberStatus($id: ID!) {
    getMemberStatus(id: $id) {
      id
      name
      owner
      comment
      status
      talkable
      image
      timestamp
    }
  }
`;
export const listMemberStatuses = /* GraphQL */ `
  query ListMemberStatuses(
    $filter: ModelMemberStatusFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listMemberStatuses(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        owner
        comment
        status
        talkable
        image
        timestamp
      }
      nextToken
    }
  }
`;
