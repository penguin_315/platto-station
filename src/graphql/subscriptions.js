/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateMemberStatus = /* GraphQL */ `
  subscription OnCreateMemberStatus {
    onCreateMemberStatus {
      id
      name
      owner
      comment
      status
      talkable
      timestamp
    }
  }
`;
export const onUpdateMemberStatus = /* GraphQL */ `
  subscription OnUpdateMemberStatus {
    onUpdateMemberStatus {
      id
      name
      owner
      comment
      status
      talkable
      timestamp
    }
  }
`;
export const onDeleteMemberStatus = /* GraphQL */ `
  subscription OnDeleteMemberStatus {
    onDeleteMemberStatus {
      id
      name
      owner
      comment
      status
      talkable
      timestamp
    }
  }
`;
