import React, { useEffect } from "react";
import { Switch, Route, useLocation } from "react-router-dom";

import "./css/style.scss";

import { focusHandling } from "cruip-js-toolkit";
import "./charts/ChartjsConfig";

// Amplify
//import logo from "./logo.svg";
//import "./App.css";
//import { withAuthenticator, AmplifySignOut } from "@aws-amplify/ui-react";

// Import pages
import Dashboard from "./pages/Dashboard";
import Profile from "./pages/Profile";
import Team from "./pages/Team";
import Feedback from "./pages/Feedback";
import Meetings from "./pages/Meetings";
import Icebreaker from "./pages/Icebreaker";
import Settings from "./pages/Settings";
import ThatsDone from "./pages/ThatsDone";

function App() {
  const location = useLocation();

  useEffect(() => {
    document.querySelector("html").style.scrollBehavior = "auto";
    window.scroll({ top: 0 });
    document.querySelector("html").style.scrollBehavior = "";
    focusHandling("outline");
  }, [location.pathname]); // triggered on route change

  return (
    <>
      <Switch>
        <Route exact path='/profile'>
          <Profile />
        </Route>
        <Route exact path='/team'>
          <Team />
        </Route>
        <Route exact path='/thatsdone'>
          <ThatsDone />
        </Route>
        <Route exact path='/meetings'>
          <Meetings />
        </Route>
        <Route exact path='/icebreaker'>
          <Icebreaker />
        </Route>
        <Route exact path='/settings'>
          <Settings />
        </Route>

        <Route exact path='/'>
          <Dashboard />
        </Route>
      </Switch>
    </>
  );
}

export default App;
// export default withAuthenticator(App);
