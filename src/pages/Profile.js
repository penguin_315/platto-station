import React, { useState } from "react";

import Sidebar from "../partials/Sidebar";
import Header from "../partials/Header";
import Banner from "../partials/Banner";
import WelcomeBannerProf from "../partials/dashboard/WelcomeBannerProf";
import ProfileCard01 from "../partials/profile/ProfileCard01";
import ProfileCard02 from "../partials/profile/ProfileCard02";

function Profile() {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  return (
    <div className='flex h-screen overflow-hidden'>
      {/* Sidebar */}
      <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />

      {/* Content area */}
      <div className='relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden'>
        {/*  Site header */}
        <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />

        <main>
          <div className='px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto'>
            {/* Welcome banner */}
            <WelcomeBannerProf />

            {/* Cards */}
            <div className='grid grid-cols-12 gap-6'>
              {/* Card (Profile) */}
              <ProfileCard01 />
            </div>
          </div>

          {/* Card (ProfileForm) */}
          <div className='grid'>
            <ProfileCard02 />
          </div>
        </main>

        <Banner />
      </div>
    </div>
  );
}

export default Profile;
