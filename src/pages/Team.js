import React, { useState } from "react";

import Sidebar from "../partials/Sidebar";
import Header from "../partials/Header";
import Banner from "../partials/Banner";
import TeamCard01 from "../partials/team/TeamCard01";
import TeamCard02 from "../partials/team/TeamCard02";

function Team() {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  return (
    <div className='flex h-screen overflow-hidden'>
      {/* Sidebar */}
      <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />

      {/* Content area */}
      <div className='relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden'>
        {/*  Site header */}
        <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />

        <main>
          <div className='px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto'>
            {/* Cards */}
            {/* <div className='grid grid-cols-12 gap-6'> */}
            {/* Card (Profile) */}
            {/* <TeamCard01 />
            </div> */}
            <div className='grid grid-rows-2 gap-6'>
              <TeamCard02 />
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}

export default Team;
