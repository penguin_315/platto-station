import React from "react";

// import Image01 from "../../images/user-avatar-32.png";
import Bingo from "../../images/icebreaker/bingo.jpg";

function IcebreakerCard01() {
  return (
    <>
      <div className='col-span-full bg-white shadow-lg rounded-sm border border-gray-200'>
        <header className='px-5 py-4 border-b border-gray-100'>
          <h2 className='font-semibold text-gray-800'>お名前ビンゴ</h2>
        </header>
        <div className='p-3'>
          {/* Table */}
          <div className='overflow-x-auto'>
            <img src={Bingo} alt='お名前ビンゴ' className='mx-auto' />
          </div>
        </div>
      </div>
    </>
  );
}

export default IcebreakerCard01;
