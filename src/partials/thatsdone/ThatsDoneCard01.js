import React from "react";

//import Image01 from "../../images/user-avatar-32.png";

function ThatsDoneCard01() {
  return (
    <>
      <div className='col-span-full bg-white shadow-lg rounded-sm border border-gray-200'>
        <header className='px-5 py-4 border-b border-gray-100'>
          <h2 className='font-semibold text-gray-800'>
            2021/9/11のミーティング
          </h2>
        </header>
        <div className='px-5 py-4'>
          <div>
            <p className='text-sm text-gray-700'>
              ミーティングお疲れさまでした！振り返りメモを保存しませんか？
            </p>
          </div>

          <div className='overflow-x-auto py-3'>
            <div className='pb-3'>
              <h3 className='font-semibold text-gray-800 pb-2'>
                話題に出たキーワード
              </h3>
              <span className='text-indigo-400'>#AWS #Amplify #Neptune</span>
            </div>

            <div className='pb-3'>
              <h3 className='font-semibold text-gray-800'>会話の比率</h3>
              <p>
                あなたは<span className='text-red-500 text-base'>20%</span>
                くらい話していたようです。
              </p>
            </div>

            <form>
              <div className='pb-3'>
                <h3 className='font-semibold text-gray-800'>楽しめた度合い</h3>
                <div className='grid grid-flow-col w-4/12 pb-2'>
                  <div className='grid-cols-2'>
                    <svg
                      id='q1-1'
                      xmlns='http://www.w3.org/2000/svg'
                      width='24'
                      height='24'
                      viewBox='0 0 24 24'
                      className='text-gray-400 fill-current'>
                      <path d='M12 5.173l2.335 4.817 5.305.732-3.861 3.71.942 5.27-4.721-2.524-4.721 2.525.942-5.27-3.861-3.71 5.305-.733 2.335-4.817zm0-4.586l-3.668 7.568-8.332 1.151 6.064 5.828-1.48 8.279 7.416-3.967 7.416 3.966-1.48-8.279 6.064-5.827-8.332-1.15-3.668-7.569z' />
                    </svg>
                  </div>
                  <div className='grid-cols-2 '>
                    <svg
                      id='q1-2'
                      xmlns='http://www.w3.org/2000/svg'
                      width='24'
                      height='24'
                      viewBox='0 0 24 24'
                      className='text-gray-400 fill-current'>
                      <path d='M12 5.173l2.335 4.817 5.305.732-3.861 3.71.942 5.27-4.721-2.524-4.721 2.525.942-5.27-3.861-3.71 5.305-.733 2.335-4.817zm0-4.586l-3.668 7.568-8.332 1.151 6.064 5.828-1.48 8.279 7.416-3.967 7.416 3.966-1.48-8.279 6.064-5.827-8.332-1.15-3.668-7.569z' />
                    </svg>
                  </div>
                  <div className='grid-cols-2 '>
                    <svg
                      id='q1-3'
                      xmlns='http://www.w3.org/2000/svg'
                      width='24'
                      height='24'
                      viewBox='0 0 24 24'
                      className='text-gray-400 fill-current'>
                      <path d='M12 5.173l2.335 4.817 5.305.732-3.861 3.71.942 5.27-4.721-2.524-4.721 2.525.942-5.27-3.861-3.71 5.305-.733 2.335-4.817zm0-4.586l-3.668 7.568-8.332 1.151 6.064 5.828-1.48 8.279 7.416-3.967 7.416 3.966-1.48-8.279 6.064-5.827-8.332-1.15-3.668-7.569z' />
                    </svg>
                  </div>
                  <div className='grid-cols-2 '>
                    <svg
                      id='q1-4'
                      xmlns='http://www.w3.org/2000/svg'
                      width='24'
                      height='24'
                      viewBox='0 0 24 24'
                      className='text-gray-400 fill-current'>
                      <path d='M12 5.173l2.335 4.817 5.305.732-3.861 3.71.942 5.27-4.721-2.524-4.721 2.525.942-5.27-3.861-3.71 5.305-.733 2.335-4.817zm0-4.586l-3.668 7.568-8.332 1.151 6.064 5.828-1.48 8.279 7.416-3.967 7.416 3.966-1.48-8.279 6.064-5.827-8.332-1.15-3.668-7.569z' />
                    </svg>
                  </div>
                  <div className='grid-cols-2 '>
                    <svg
                      id='q1-5'
                      xmlns='http://www.w3.org/2000/svg'
                      width='24'
                      height='24'
                      viewBox='0 0 24 24'
                      className='text-gray-400 fill-current'>
                      <path d='M12 5.173l2.335 4.817 5.305.732-3.861 3.71.942 5.27-4.721-2.524-4.721 2.525.942-5.27-3.861-3.71 5.305-.733 2.335-4.817zm0-4.586l-3.668 7.568-8.332 1.151 6.064 5.828-1.48 8.279 7.416-3.967 7.416 3.966-1.48-8.279 6.064-5.827-8.332-1.15-3.668-7.569z' />
                    </svg>
                  </div>
                </div>
              </div>
              <div className='pb-3'>
                <h3 className='font-semibold text-gray-800 py-2'>会話のメモ</h3>
                <textarea rows='3' className='w-full pb-3'></textarea>
                <button className='rounded-md inline-flex justify-center py-2 px-4 mt-3 text-sm text-white bg-indigo-400 hover:bg-indigo-500'>
                  保存する
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default ThatsDoneCard01;
