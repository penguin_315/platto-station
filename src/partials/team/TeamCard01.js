import React from "react";

import Image01 from "../../images/user-avatar-32.png";
import Image02 from "../../images/user-36-06.jpg";
import Image03 from "../../images/user-36-07.jpg";
import Image04 from "../../images/user-36-08.jpg";
import Image05 from "../../images/user-36-09.jpg";
import Interest from "../../images/interest.jpg";

function TeamCard01() {
  const customers = [
    {
      id: "0",
      image: Image01,
      name: "上條さん",
      role: "プロジェクトマネージャー",
      location: "東京",
      is_busy: "いつでも会話OK",
      color: "text-green-500",
    },
    {
      id: "1",
      image: Image02,
      name: "斎藤さん",
      role: "テクニカルリーダー",
      location: "東京",
      is_busy: "午前中は集中タイム",
      color: "text-yellow-600",
    },
    {
      id: "2",
      image: Image03,
      name: "牧野さん",
      role: "バックエンドエンジニア",
      location: "大阪",
      is_busy: "13:00～14:00不在",
      color: "text-yellow-600",
    },
    {
      id: "3",
      image: Image04,
      name: "亀山さん",
      role: "カスタマーサポート",
      location: "札幌",
      is_busy: "外出中",
      color: "text-red-900",
    },
    {
      id: "4",
      image: Image05,
      name: "越智さん",
      role: "フロントエンドエンジニア",
      location: "沖縄",
      is_busy: "いつでも会話OK",
      color: "text-green-500",
    },
  ];

  return (
    <>
      <div className='col-span-full bg-white shadow-lg rounded-sm border border-gray-200'>
        <header className='px-5 py-4 border-b border-gray-100'>
          <h2 className='font-semibold text-gray-800'>platto-stationチーム</h2>
        </header>
        <div className='p-3'>
          {/* Table */}
          <div className='overflow-x-auto'>
            <table className='table-auto w-full'>
              {/* Table header */}
              <thead className='text-xs font-semibold uppercase text-gray-400 bg-gray-50'>
                <tr>
                  <th className='p-2 whitespace-nowrap'>
                    <div className='font-semibold text-left'>氏名</div>
                  </th>
                  <th className='p-2 whitespace-nowrap'>
                    <div className='font-semibold text-left'>担当</div>
                  </th>
                  <th className='p-2 whitespace-nowrap'>
                    <div className='font-semibold text-left'>状況</div>
                  </th>
                  <th className='p-2 whitespace-nowrap'>
                    <div className='font-semibold text-left'>所在地</div>
                  </th>
                </tr>
              </thead>
              {/* Table body */}
              <tbody className='text-sm divide-y divide-gray-100'>
                {customers.map((customer) => {
                  return (
                    <tr key={customer.id}>
                      <td className='p-2 whitespace-nowrap'>
                        <div className='flex items-center'>
                          <div className='w-10 h-10 flex-shrink-0 mr-2 sm:mr-3'>
                            <img
                              className='rounded-full'
                              src={customer.image}
                              width='40'
                              height='40'
                              alt={customer.name}
                            />
                          </div>
                          <div className='font-medium text-gray-800'>
                            {customer.name}
                          </div>
                        </div>
                      </td>
                      <td className='p-2 whitespace-nowrap'>
                        <div className='text-left'>{customer.role}</div>
                      </td>
                      <td className='p-2 whitespace-nowrap'>
                        <div className={customer.color}>
                          {/*text-green-500*/}
                          {customer.is_busy}
                        </div>
                      </td>
                      <td className='p-2 whitespace-nowrap'>
                        {/* <div className='text-lg text-center'> */}
                        {customer.location}
                        {/* </div> */}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}

export default TeamCard01;
