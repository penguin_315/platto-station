import React from "react";

import Image01 from "../../images/user-avatar-32.png";
import Image02 from "../../images/user-36-06.jpg";
import Image03 from "../../images/user-36-07.jpg";
import Image04 from "../../images/user-36-08.jpg";
import Image05 from "../../images/user-36-09.jpg";
import Interest from "../../images/interest.jpg";

function TeamCard02() {
  const customers = [
    {
      id: "0",
      image: Image01,
      name: "テスト　ユーザー",
      role: "プロジェクトマネージャー",
      location: "東京",
      is_busy: "いつでも会話OK",
      color: "text-green-500",
    },
    {
      id: "1",
      image: Image02,
      name: "テスト　ユーザー2",
      role: "テクニカルリーダー",
      location: "東京",
      is_busy: "午前中は集中タイム",
      color: "text-yellow-600",
    },
    {
      id: "2",
      image: Image03,
      name: "テスト　ユーザー3",
      role: "バックエンドエンジニア",
      location: "大阪",
      is_busy: "13:00～14:00不在",
      color: "text-yellow-600",
    },
    {
      id: "3",
      image: Image04,
      name: "テスト　ユーザー4",
      role: "カスタマーサポート",
      location: "札幌",
      is_busy: "外出中",
      color: "text-red-900",
    },
    {
      id: "4",
      image: Image05,
      name: "テスト　ユーザー5",
      role: "フロントエンドエンジニア",
      location: "沖縄",
      is_busy: "いつでも会話OK",
      color: "text-green-500",
    },
  ];

  return (
    <>
      <section class='text-gray-600 body-font'>
        <div class='container px-5 py-2 mx-auto'>
          <div class='flex flex-wrap -m-4'>
            <div class='xl:w-1/3 md:w-1/2 p-4'>
              <div class='border border-gray-200 p-6 rounded-lg'>
                <div class='w-10 h-10 inline-flex items-center justify-center rounded-full bg-indigo-100 text-indigo-500 mb-4'>
                  <div className='flex items-center'>
                    <div className='w-10 h-10 flex-shrink-0 mr-2 sm:mr-3'>
                      <img
                        className='rounded-full'
                        src={Image01}
                        width='40'
                        height='40'
                        alt='test'
                      />
                    </div>
                  </div>
                </div>
                <h2 class='text-lg text-gray-900 font-medium title-font mb-2'>
                  亀山　鶴海さん
                </h2>
                <p class='leading-relaxed text-base'>
                  東京出身です。最近ヨガ教室に通い始めました。
                  <br />
                  前職はIT企業で営業を担当していました。
                </p>
                <div className='flex-shrink-0 self-end ml-2 text-right'>
                  <a
                    className='text-sm text-indigo-500 hover:text-indigo-600'
                    href='#0'>
                    詳細<span className='hidden sm:inline'> -&gt;</span>
                  </a>
                </div>
              </div>
            </div>
            <div class='xl:w-1/3 md:w-1/2 p-4'>
              <div class='border border-gray-200 p-6 rounded-lg'>
                <div class='w-10 h-10 inline-flex items-center justify-center rounded-full bg-indigo-100 text-indigo-500 mb-4'>
                  <div className='flex items-center'>
                    <div className='w-10 h-10 flex-shrink-0 mr-2 sm:mr-3'>
                      <img
                        className='rounded-full'
                        src={Image02}
                        width='40'
                        height='40'
                        alt='test'
                      />
                    </div>
                  </div>
                </div>
                <h2 class='text-lg text-gray-900 font-medium title-font mb-2'>
                  越智　昇さん
                </h2>
                <p class='leading-relaxed text-base'>
                  2021年6月入社です。
                  <br />
                  ボルダリングが趣味です。是非一緒に行きましょう！
                </p>
                <div className='flex-shrink-0 self-end ml-2 text-right'>
                  <a
                    className='text-sm text-indigo-500 hover:text-indigo-600'
                    href='#0'>
                    詳細<span className='hidden sm:inline'> -&gt;</span>
                  </a>
                </div>
              </div>
            </div>
            <div class='xl:w-1/3 md:w-1/2 p-4'>
              <div class='border border-gray-200 p-6 rounded-lg'>
                <div class='w-10 h-10 inline-flex items-center justify-center rounded-full bg-indigo-100 text-indigo-500 mb-4'>
                  <div className='flex items-center'>
                    <div className='w-10 h-10 flex-shrink-0 mr-2 sm:mr-3'>
                      <img
                        className='rounded-full'
                        src={Image03}
                        width='40'
                        height='40'
                        alt='test'
                      />
                    </div>
                  </div>
                </div>
                <h2 class='text-lg text-gray-900 font-medium title-font mb-2'>
                  白鳥　黒子さん
                </h2>
                <p class='leading-relaxed text-base'>よろしくお願いします！</p>
                <div className='flex-shrink-0 self-end ml-2 text-right'>
                  <a
                    className='text-sm text-indigo-500 hover:text-indigo-600'
                    href='#0'>
                    詳細<span className='hidden sm:inline'> -&gt;</span>
                  </a>
                </div>
              </div>
            </div>
            <div class='xl:w-1/3 md:w-1/2 p-4'>
              <div class='border border-gray-200 p-6 rounded-lg'>
                <div class='w-10 h-10 inline-flex items-center justify-center rounded-full bg-indigo-100 text-indigo-500 mb-4'>
                  <div className='flex items-center'>
                    <div className='w-10 h-10 flex-shrink-0 mr-2 sm:mr-3'>
                      <img
                        className='rounded-full'
                        src={Image04}
                        width='40'
                        height='40'
                        alt='test'
                      />
                    </div>
                  </div>
                </div>
                <h2 class='text-lg text-gray-900 font-medium title-font mb-2'>
                  佐藤　章由さん
                </h2>
                <p class='leading-relaxed text-base'>
                  猫を2匹飼っています。SA資格取得に向けてAWSを勉強中です。
                </p>
                <div className='flex-shrink-0 self-end ml-2 text-right'>
                  <a
                    className='text-sm text-indigo-500 hover:text-indigo-600'
                    href='#0'>
                    詳細<span className='hidden sm:inline'> -&gt;</span>
                  </a>
                </div>
              </div>
            </div>
            <div class='xl:w-1/3 md:w-1/2 p-4'>
              <div class='border border-gray-200 p-6 rounded-lg'>
                <div class='w-10 h-10 inline-flex items-center justify-center rounded-full bg-indigo-100 text-indigo-500 mb-4'>
                  <div className='flex items-center'>
                    <div className='w-10 h-10 flex-shrink-0 mr-2 sm:mr-3'>
                      <img
                        className='rounded-full'
                        src={Image05}
                        width='40'
                        height='40'
                        alt='test'
                      />
                    </div>
                  </div>
                </div>
                <h2 class='text-lg text-gray-900 font-medium title-font mb-2'>
                  辺銀　菜香さん
                </h2>
                <p class='leading-relaxed text-base'>
                  基本的に自宅勤務をしています。作業中の会話が好きなので、お気軽に話しかけてくださいね。
                </p>
                <div className='flex-shrink-0 self-end ml-2 text-right'>
                  <a
                    className='text-sm text-indigo-500 hover:text-indigo-600'
                    href='#0'>
                    詳細<span className='hidden sm:inline'> -&gt;</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default TeamCard02;
