import React from "react";

function ProfileCard02() {
  return (
    <>
      <div className='mt-10 sm:mt-0 md:grid-cols-10 md:gap-5 px-8'>
        <header className='px-4 py-5 bg-white border-gray-100'>
          <h2 className='font-semibold text-gray-800'>プロフィール入力</h2>
        </header>
        {/* <div className='md:grid md:grid-cols-3 md:gap-6'> */}
        {/* <div className='md:col-span-1'></div> */}
        {/* <div className='mt-5 md:mt-0 md:col-span-2'> */}
        <form action='#' method='POST'>
          <div className='shadow overflow-hidden sm:rounded-md'>
            <div className='px-4 py-5 bg-white sm:p-6'>
              <div className='grid grid-cols-6 gap-6'>
                <div className='col-span-6 sm:col-span-6 lg:col-span-6'>
                  <h2 className='bg-gray-200 rounded px-5 py-2 border-b border-gray-100'>
                    基本情報
                    <input
                      id='remember_me'
                      name='remember_me'
                      type='checkbox'
                      className='h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded content-end ml-24'
                    />
                    <label
                      for='remember_me'
                      className='ml-2 text-sm text-gray-900'>
                      公開
                    </label>
                  </h2>
                </div>
                <div className='col-span-6 sm:col-span-6 lg:col-span-2'>
                  <label
                    for='first_name'
                    className='block text-sm font-medium text-gray-700'>
                    名字
                  </label>
                  <input
                    type='text'
                    name='first_name'
                    id='first_name'
                    autocomplete='given-name'
                    className='mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md'
                  />
                </div>
                <div className='col-span-6 sm:col-span-6 lg:col-span-2'>
                  <label
                    for='first_name'
                    className='block text-sm font-medium text-gray-700'>
                    名前
                  </label>
                  <input
                    type='text'
                    name='first_name'
                    id='first_name'
                    autocomplete='given-name'
                    className='mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md'
                  />
                </div>

                <div className='col-span-6 sm:col-span-4'>
                  <label
                    for='email_address'
                    className='block text-sm font-medium text-gray-700'>
                    社内アドレス
                  </label>
                  <input
                    type='text'
                    name='email_address'
                    id='email_address'
                    autocomplete='email'
                    className='mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md'
                  />
                </div>
                <div className='col-span-6 sm:col-span-6 lg:col-span-6'>
                  <h2 className='bg-gray-200 rounded px-5 py-2 border-b border-gray-100'>
                    基本情報2
                    <input
                      id='remember_me'
                      name='remember_me'
                      type='checkbox'
                      className='h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded content-end ml-24'
                    />
                    <label
                      for='remember_me'
                      className='ml-2 text-sm text-gray-900'>
                      公開
                    </label>
                  </h2>
                </div>
                <div className='col-span-6 sm:col-span-3'>
                  <label
                    for='country'
                    className='block text-sm font-medium text-gray-700'>
                    国 / 地域
                  </label>
                  <select
                    id='country'
                    name='country'
                    autocomplete='country'
                    className='mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm'>
                    <option>日本</option>
                    <option>大阪</option>
                    <option>アメリカ</option>
                    <option>シンガポール</option>
                  </select>
                </div>

                <div className='col-span-6 sm:col-span-6 lg:col-span-2'>
                  <label
                    for='city'
                    className='block text-sm font-medium text-gray-700'>
                    都市
                  </label>
                  <input
                    type='text'
                    name='city'
                    id='city'
                    className='mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md'
                  />
                </div>
                <div className='col-span-6 sm:col-span-6 lg:col-span-6'>
                  <h2 className='bg-gray-200 rounded px-5 py-2 border-b border-gray-100'>
                    興味のある分野
                    <input
                      id='remember_me'
                      name='remember_me'
                      type='checkbox'
                      className='h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded content-end ml-24'
                    />
                    <label
                      for='remember_me'
                      className='ml-2 text-sm text-gray-900'>
                      公開
                    </label>
                  </h2>
                </div>
                <div className='col-span-6 sm:col-span-3'>
                  <label
                    for='country'
                    className='block text-sm font-medium text-gray-700'>
                    好きなAWSサービス
                  </label>
                  <select
                    id='country'
                    name='country'
                    autocomplete='country'
                    className='mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm'>
                    <option>IAM</option>
                    <option>S3</option>
                    <option>CloudFormation</option>
                    <option>EC2</option>
                    <option>etc...</option>
                  </select>
                </div>

                <div className='col-span-6 sm:col-span-6 lg:col-span-2'>
                  <label
                    for='city'
                    className='block text-sm font-medium text-gray-700'>
                    オススメの本
                  </label>
                  <input
                    type='text'
                    name='city'
                    id='city'
                    className='mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md'
                  />
                </div>

                <div className='col-span-6 sm:col-span-6 lg:col-span-6'>
                  <h2 className='bg-gray-200 rounded px-5 py-2 border-b border-gray-100'>
                    基本情報3
                    <input
                      id='remember_me'
                      name='remember_me'
                      type='checkbox'
                      className='h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded content-end ml-24'
                    />
                    <label
                      for='remember_me'
                      className='ml-2 text-sm text-gray-900'>
                      公開
                    </label>
                  </h2>
                </div>
                <div className='col-span-6'>
                  <label
                    for='street_address'
                    className='block text-sm font-medium text-gray-700'>
                    ひとこと
                  </label>
                  <input
                    type='text'
                    name='street_address'
                    id='street_address'
                    autocomplete='street-address'
                    className='mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md'
                  />
                </div>
              </div>
            </div>
            <div className='px-4 py-3 bg-gray-50 text-center sm:px-6'>
              <button
                type='submit'
                className='inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'>
                保存
              </button>
            </div>
          </div>
        </form>
        {/* </div> */}
        {/* </div> */}
      </div>
    </>
  );
}

export default ProfileCard02;
