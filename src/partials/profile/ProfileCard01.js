import React from "react";

import Image01 from "../../images/user-avatar-32.png";
import Image02 from "../../images/user-36-06.jpg";
import Image03 from "../../images/user-36-07.jpg";
import Image04 from "../../images/user-36-08.jpg";
import Image05 from "../../images/user-36-09.jpg";
import Interest from "../../images/interest.jpg";

function ProfileCard01() {
  return (
    <>
      <div className='col-span-full bg-white shadow-lg rounded-sm border border-gray-200'>
        <header className='px-5 py-4 border-b border-gray-100'>
          <h2 className='font-semibold text-gray-800'>興味のつながり</h2>
        </header>
        <div className='p-3'>
          <div className='pb-1'>
            <form>
              <input
                className='px-4 py-2 text-sm'
                type='text'
                placeholder='キーワード検索'
              />
              <button className='ml-2 rounded-md inline-flex justify-center py-2 px-4 text-sm text-white bg-indigo-400 hover:bg-indigo-500'>
                検索
              </button>
            </form>
          </div>
          {/* Table */}
          <div className='overflow-x-auto'>
            <img
              src={Interest}
              alt='興味のつながり'
              className='w-6/12 mx-auto'
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default ProfileCard01;
