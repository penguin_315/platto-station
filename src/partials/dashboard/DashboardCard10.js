import React, { useState, useEffect, useRef } from "react";
import { API, Storage } from "aws-amplify";
import { listMemberStatuses } from "../../graphql/queries";
import { updateMemberStatus } from "../../graphql/mutations";

import EditIcon from "../../images/icon-edit.svg";

import Image01 from "../../images/user-avatar-32.png";
import Image02 from "../../images/user-36-06.jpg";
import Image03 from "../../images/user-36-07.jpg";
import Image04 from "../../images/user-36-08.jpg";
import Image05 from "../../images/user-36-09.jpg";
import Kaiwa01 from "../../images/kaiwa-welcome.png";
import Kaiwa02 from "../../images/kaiwa-ok.png";
import Kaiwa03 from "../../images/kaiwa-soso.png";
import Kaiwa04 from "../../images/kaiwa-no.png";
import Kaiwa05 from "../../images/kaiwa-ng.png";

function DashboardCard10() {
  const [memberStatus, setMemberStatus] = useState([]);
  const [isEdit, setIsEdit] = useState(false);
  const [modTarget, setModTarget] = useState("");
  // const fileInputRef = useRef(null);

  useEffect(() => {
    fetchMemberStatus();
  }, []);

  const fetchMemberStatus = async () => {
    const apiData = await API.graphql({
      query: listMemberStatuses,
      variables: { sortDirection: "ASC" },
    });

    const StatusFromAPI = apiData.data.listMemberStatuses.items;
    // await Promise.all(
    //   StatusFromAPI.map(async (status) => {
    //     if (status.image) {
    //       const image = await Storage.get(status.image);
    //       console.log("image_data", image);
    //       status.image = image;
    //     }
    //     return status;
    //   })
    // );

    setMemberStatus(apiData.data.listMemberStatuses.items);
    console.log("data", apiData.data.listMemberStatuses.items);
  };

  const handleClick = (member_id, idx) => {
    setIsEdit(!isEdit);
    setModTarget(idx);

    //編集モードを終了する場合のみ、DB更新
    if (isEdit !== false) {
      handleUpdateMemberStatus(member_id, idx);
    }
  };

  const handleUpdateMemberStatus = async (member_id, idx) => {
    console.log("event", member_id, idx);

    await API.graphql({
      query: updateMemberStatus,
      variables: { input: { ...memberStatus[idx] } },
    });
  };

  const handleCommentChange = (val, member_id) => {
    console.log("comment", val);

    const tmp_status = memberStatus.map((status) => {
      if (status.id === member_id) {
        return { ...status, comment: val };
      } else {
        return status;
      }
    });

    setMemberStatus(tmp_status);
  };

  const handleStatusChange = (val, member_id) => {
    console.log("status", val);

    const tmp_status = memberStatus.map((status) => {
      if (status.id === member_id) {
        return { ...status, status: val };
      } else {
        return status;
      }
    });

    setMemberStatus(tmp_status);
  };

  const handleImageChange = (member_id, image_id) => {
    if (!isEdit) return;

    const imgIndex = image_id > 4 ? 1 : Number(image_id) + 1;

    const tmp_status = memberStatus.map((status) => {
      if (status.id === member_id) {
        console.log("hit", status);
        return { ...status, talkable: imgIndex };
      } else {
        return status;
      }
    });

    setMemberStatus(tmp_status);
  };

  const handleUploadImage = async (img, member_id) => {
    if (!img.target.files[0]) return;
    console.log("this", img.target.files[0]);

    // const file = img.target.files[0];
    const file = img.target.files[0];

    const tmp_status = memberStatus.map((status) => {
      if (status.id === member_id) {
        return { ...status, image: file.name };
      } else {
        return status;
      }
    });

    setMemberStatus(tmp_status);
    await Storage.put(file.name, file);
  };

  const handleModImageSrc = (img) => {
    switch (Number(img)) {
      case 1:
        return Kaiwa01;

      case 2:
        return Kaiwa02;

      case 3:
        return Kaiwa03;

      case 4:
        return Kaiwa04;

      case 5:
        return Kaiwa05;

      default:
        return Kaiwa01;
    }
  };

  return (
    <div className='col-span-full bg-white shadow-lg rounded-sm border border-gray-200'>
      <header className='px-5 py-4 border-b border-gray-100'>
        {/* <h2 className='font-semibold text-gray-800'>platto-stationチーム</h2> */}
        <select
          name='team'
          autocomplete='team'
          className='mt-1 w-1/2 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm font-semibold text-gray-800'>
          <option>platto-stationチーム</option>
          <option>大阪チーム</option>
          <option>アメリカチーム</option>
          <option>シンガポールチーム</option>
        </select>
      </header>
      <div className='p-3'>
        {/* Table */}
        <div className='overflow-x-auto'>
          <table className='table-auto w-full'>
            {/* Table header */}
            <thead className='text-xs font-semibold uppercase text-gray-400 bg-gray-50'>
              <tr>
                <th className='p-2 whitespace-nowrap'>
                  <div className='font-semibold text-left'>氏名</div>
                </th>
                <th className='p-2 whitespace-nowrap'>
                  <div className='font-semibold text-left'>ひとこと</div>
                </th>
                <th className='p-2 whitespace-nowrap'>
                  <div className='font-semibold text-left'>予定</div>
                </th>
                <th className='p-2 whitespace-nowrap'>
                  <div className='font-semibold text-left'>ステータス</div>
                </th>
                <th className='p-2 whitespace-nowrap'>編集</th>
              </tr>
            </thead>
            {/* Table body */}
            <tbody className='text-sm divide-y divide-gray-100'>
              {memberStatus.map((member, idx) => {
                return (
                  <tr key={member.id}>
                    <td className='p-2 whitespace-nowrap'>
                      <div className='flex items-center'>
                        <div className='w-10 h-10 flex-shrink-0 mr-2 sm:mr-3'>
                          <img
                            className='rounded-full'
                            src={member.image ? member.image : Image01}
                            width='40'
                            height='40'
                            alt={member.name}
                          />
                          {
                            // isEdit === true && modTarget === idx && (
                            //   <input
                            //     type='file'
                            //     onChange={(e) => handleUploadImage(e, member.id)}
                            //   />
                            // )
                          }
                          {/* <input
                            type='file'
                            hidden
                            accept='image/*'
                            ref={fileInputRef}
                            onChange={handleUploadImage(this, member.id)}
                          /> */}
                        </div>
                        <div className='font-medium text-gray-800'>
                          {member.name}
                        </div>
                      </div>
                    </td>
                    <td className='p-2 whitespace-nowrap'>
                      <div className='text-left'>
                        <input
                          className={
                            isEdit === true && modTarget === idx
                              ? "text-sm"
                              : "border-0 text-sm"
                          }
                          name='comment'
                          type='text'
                          defaultValue={member.comment}
                          onBlur={(e) =>
                            handleCommentChange(e.target.value, member.id)
                          }
                          disabled={!isEdit}
                        />
                      </div>
                    </td>
                    <td className='p-2 whitespace-nowrap'>
                      <input
                        className={
                          isEdit === true && modTarget === idx
                            ? "text-sm"
                            : "border-0 text-sm"
                        }
                        name='status'
                        type='text'
                        defaultValue={member.status}
                        onBlur={(e) =>
                          handleStatusChange(e.target.value, member.id)
                        }
                        disabled={!isEdit}
                      />
                    </td>
                    <td className='p-2'>
                      <img
                        src={handleModImageSrc(member.talkable)}
                        alt='status'
                        onClick={(e) => {
                          handleImageChange(member.id, member.talkable);
                        }}
                      />

                      {/* {isEdit && (
                        <select name='talkable' className='text-sm'>
                          <option selected value='Kaiwa01'>
                            いつでもOK
                          </option>
                          <option value='Kaiwa02'>会話OK</option>
                          <option value='Kaiwa03'>お声がけください</option>
                          <option value='Kaiwa04'>集中したいです</option>
                          <option value='Kaiwa05'>会話NG</option>
                        </select>
                      )} */}
                    </td>
                    <td className='p-2'>
                      <img
                        onClick={(e) => handleClick(member.id, idx)}
                        className={
                          isEdit === true && modTarget === idx
                            ? "text-sm w-5 text-indigo-800"
                            : "text-sm w-5"
                        }
                        src={EditIcon}
                        alt='Edit'
                        name={idx}
                      />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default DashboardCard10;
