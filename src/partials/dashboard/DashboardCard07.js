import React from "react";

function DashboardCard07() {
  return (
    <div className='col-span-full xl:col-span-12 bg-white shadow-lg rounded-sm border border-gray-200'>
      <header className='px-5 py-4 border-b border-gray-100'>
        <h2 className='font-semibold text-gray-800'>タスク一覧</h2>
      </header>
      <div className='p-3 overflow-x-auto'>
        <iframe
          className='w-full h-screen'
          src='https://docs.google.com/spreadsheets/d/e/2PACX-1vQIo8DnC8lOiB3tCy37kZ-MyxDnkh65Bli9O-Hz83sk-L_MJUW-Y_tTOWzg302JxpPelzdKTVPxN4L1/pubhtml?widget=true&amp;headers=false'></iframe>
      </div>
    </div>
  );
}

export default DashboardCard07;
